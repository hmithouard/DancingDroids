//AUTHORS :
//Hippolyte MITHOUARD 🐸
//Loufti HINOUN
//Rabah Debbah

#[macro_use]
extern crate assert_matches;

use std::fs;
use std::fs::File;
use std::io;
use std::io::prelude::*;

#[derive(Debug, Clone)]
struct Robot {
    id: i32,
    pos_x: i32,
    pos_y: i32,
    orientation: Orientation,
    instructions: Vec<Action>,
}
#[derive(Debug, Clone)]
struct Robots {
    liste: Vec<Robot>,
}
#[derive(Debug, Copy, Clone)]
struct Grille {
    longueur: i32,
    largeur: i32,
}
#[derive(Debug, Copy, Clone)]
enum Orientation {
    North,
    South,
    East,
    West,
}

impl Orientation {
    //Fonction qui prend un &str et renvoie une enum Orientation.
    //Revoie une Orientation North pars défaut ou si il y a une erreur
    fn from_str(o: &str) -> Orientation {
        let orientation = match o {
            "N" => Orientation::North,
            "S" => Orientation::South,
            "E" => Orientation::East,
            "W" => Orientation::West,
            _ => Orientation::North,
        };
        return orientation;
    }
}
#[derive(Debug, Copy, Clone)]
enum Action {
    L,
    R,
    F,
}

impl Action {
    fn from_str(o: char) -> Action {
        let action = match o {
            'F' => Action::F,
            'R' => Action::R,
            'L' => Action::L,
            _ => Action::F,
        };
        return action;
    }
}
//Fonction qui gère le déplacement et la collision entre des robots
//Cette fonction prend en paramètre la liste de robots à déplacer et la grille de jeu pour avoir les limites.
fn deplacement(grille: &Grille, robs: &mut Robots) {
    let mut collision: i32;
    let mut robs_bis = robs.clone();
    for mut rob in &mut robs.liste {
        for y in rob.instructions.iter() {
            match y {
                Action::L => match rob.orientation {
                    Orientation::North => rob.orientation = Orientation::West,
                    Orientation::South => rob.orientation = Orientation::East,
                    Orientation::East => rob.orientation = Orientation::North,
                    Orientation::West => rob.orientation = Orientation::South,
                },
                Action::R => match rob.orientation {
                    Orientation::North => rob.orientation = Orientation::East,
                    Orientation::South => rob.orientation = Orientation::West,
                    Orientation::East => rob.orientation = Orientation::South,
                    Orientation::West => rob.orientation = Orientation::North,
                },
                Action::F => match rob.orientation {
                    Orientation::North => {
                        if rob.pos_y >= 0 {
                            collision = 0;
                            for i in robs_bis.liste.iter() {
                                if rob.pos_x == i.pos_x {
                                    if rob.pos_y - 1 == i.pos_y {
                                        collision = 1;
                                    }
                                }
                            }
                            if collision == 0 {
                                rob.pos_y = rob.pos_y - 1;
                                //Update du clone des robots
                                for mut i in &mut robs_bis.liste {
                                    if i.id == rob.id {
                                        i.pos_y = i.pos_y - 1;
                                    }
                                }
                            } else {
                                println!(
                                    "Robot ID<{}> Collision en ({}, {})\n",
                                    rob.id,
                                    rob.pos_x,
                                    rob.pos_y - 1
                                )
                            };
                        }
                    }
                    Orientation::South => {
                        if rob.pos_y < grille.largeur {
                            collision = 0;
                            for i in robs_bis.liste.iter() {
                                if rob.pos_x == i.pos_x {
                                    if rob.pos_y + 1 == i.pos_y {
                                        collision = 1;
                                    }
                                }
                            }
                            if collision == 0 {
                                rob.pos_y = rob.pos_y + 1;
                                //Update du clone des robots
                                for mut i in &mut robs_bis.liste {
                                    if i.id == rob.id {
                                        i.pos_y = i.pos_y + 1;
                                    }
                                }
                            } else {
                                println!(
                                    "Robot ID<{}> Collision en ({}, {})\n",
                                    rob.id,
                                    rob.pos_x,
                                    rob.pos_y + 1
                                )
                            };
                        }
                    }
                    Orientation::East => {
                        if rob.pos_x < grille.longueur {
                            collision = 0;
                            for i in robs_bis.liste.iter() {
                                if rob.pos_y == i.pos_y {
                                    if rob.pos_x + 1 == i.pos_x {
                                        collision = 1;
                                    }
                                }
                            }
                            if collision == 0 {
                                rob.pos_x = rob.pos_x + 1;
                                //Update du clone des robots
                                for mut i in &mut robs_bis.liste {
                                    if i.id == rob.id {
                                        i.pos_x = i.pos_x + 1;
                                    }
                                }
                            } else {
                                println!(
                                    "Robot ID<{}> Collision en ({}, {})\n",
                                    rob.id,
                                    rob.pos_x + 1,
                                    rob.pos_y
                                )
                            };
                        }
                    }
                    Orientation::West => {
                        if rob.pos_x > 0 {
                            collision = 0;
                            for i in robs_bis.liste.iter() {
                                if rob.pos_y == i.pos_y {
                                    if rob.pos_x - 1 == i.pos_x {
                                        collision = 1;
                                    }
                                }
                            }
                            if collision == 0 {
                                rob.pos_x = rob.pos_x - 1;
                                //Update du clone des robots
                                for mut i in &mut robs_bis.liste {
                                    if i.id == rob.id {
                                        i.pos_x = i.pos_x - 1;
                                    }
                                }
                            } else {
                                println!(
                                    "Robot ID<{}> Collision en ({}, {})\n",
                                    rob.id,
                                    rob.pos_x - 1,
                                    rob.pos_y
                                )
                            };
                        }
                    }
                },
                _ => (),
            }
        }
    }
}
//REMIX : Fonction du groupe de Martin HART car j'ai appris le parsing via leurs code, j'ai rajouter de quoi utilisier des Struct::Action et pas des char.
//prend le nom du fichier et le Vec contenan les robots et renvoie une Grille qui représente le monde.
fn setup(file: String, rbs: &mut Robots) -> Result<Grille, String> {
    let mut rob_id: i32 = 0;
    let mut lines = file.lines();
    let line: &str = match lines.next() {
        Some(r) => r,
        None => return Err(String::from("Impossible de lire la ligne\n")),
    };
    let mut gr_pos = line.split_whitespace();
    let gr_p1 = match gr_pos.next() {
        Some(r) => r,
        None => return Err(String::from("Impossible de lire la ligne\n")),
    };
    let gr_p2 = match gr_pos.next() {
        Some(r) => r,
        None => return Err(String::from("Impossible de lire la ligne\n")),
    };
    let x: i32 = match gr_p1.parse::<i32>() {
        Ok(x) => x,
        Err(_) => return Err(String::from("Impossible de convertir en i32 1\n")),
    };
    let y: i32 = match gr_p2.parse::<i32>() {
        Ok(y) => y,
        Err(_) => return Err(String::from("Impossible de convertir en i32 2\n")),
    };
    let gr = Grille {
        largeur: x,
        longueur: y,
    };
    loop {
        rob_id = rob_id + 1;
        let _e_ligne = match lines.next() {
            Some(r) => r,
            None => break,
        };
        let r_s_robot = match lines.next() {
            Some(r) => r,
            None => return Err(String::from("Impossible de lire la ligne\n")),
        };
        let r_inst = match lines.next() {
            Some(r) => r,
            None => return Err(String::from("Impossible de lire la ligne\n")),
        };
        let mut s_robot = r_s_robot.split_whitespace();
        let rb_x = match s_robot.next() {
            Some(r) => r,
            None => return Err(String::from("Impossible de lire la ligne\n")),
        };
        let rb_y = match s_robot.next() {
            Some(r) => r,
            None => return Err(String::from("Impossible de lire la ligne\n")),
        };
        let ori = match s_robot.next() {
            Some(r) => r,
            None => return Err(String::from("Impossible de lire la ligne\n")),
        };
        let rb_px: i32 = match rb_x.parse::<i32>() {
            Ok(x) => x,
            Err(_) => return Err(String::from("Impossible de convertir en i32\n")),
        };
        let rb_py: i32 = match rb_y.parse::<i32>() {
            Ok(y) => y,
            Err(_) => return Err(String::from("Impossible de convertir en i32\n")),
        };
        let r_o: Orientation = Orientation::from_str(ori);

        //transforme la ligne d'instruction en Vec<Action>
        let instructions: Vec<Action> = r_inst.chars().map(|i| Action::from_str(i)).collect();
        
        let rb = Robot {
            orientation: r_o,
            id: rob_id,
            pos_x: rb_px,
            pos_y: rb_py,
            instructions: instructions,
        };
        rbs.liste.push(rb);
    }

    Ok(gr)
}
// fonction d'affichage prend une référence de la grille de jeu et du vecteur de robots.
fn graphique(grille: &Grille, robs: &Robots) {
    // ligne de fin
    let mut ligne_f: String = " ".to_string();
    for i in 0..grille.largeur {
        let mut c_ligne: String = i.to_string();
        ligne_f.push_str(&i.to_string());
        ligne_f.push_str(" ");
        for j in 0..grille.longueur {
            //detecte si une position correspond à un robot pour évité de trop print ". "
            let mut r_f = 0;
            for rob in &robs.liste {
                if &rob.pos_y == &i {
                    if &rob.pos_x == &j {
                        r_f = 1;
                        match &rob.orientation {
                            Orientation::North => c_ligne.push_str("⬆️ "),
                            Orientation::South => c_ligne.push_str("⬇️ "),
                            Orientation::East => c_ligne.push_str("➡️ "),
                            Orientation::West => c_ligne.push_str("⬅️ "),
                            _ => c_ligne.push_str(". "),
                        };
                    }
                }
            }
            if r_f == 0 {
                c_ligne.push_str(". ");
            }
        }
        println!("{}", c_ligne);
    }
    println!("{}", ligne_f);
}
fn main() {
    let mut robs = Robots { liste: Vec::new() };

    let mut file = File::open("param.txt").expect("Unable to open the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Unable to read the file");

    let map: Grille = match setup(contents, &mut robs) {
        Ok(g) => g,
        Err(e) => panic!(e),
    };

    println!(
        "Dancing robots 🤖 : \nCarte de taille : {} {}",
        map.largeur, map.longueur
    );
    for rob in &robs.liste {
        println!("{:?}", rob);
    }
    println!("état initilal :\n===========================");
    graphique(&map, &robs);
    deplacement(&map, &mut robs);
    println!("état final :\n===========================");
    graphique(&map, &robs);
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_orientation() {
        assert_matches!(Orientation::from_str("N"), Orientation::North);
        assert_matches!(Orientation::from_str("S"), Orientation::South);
        assert_matches!(Orientation::from_str("E"), Orientation::East);
        assert_matches!(Orientation::from_str("W"), Orientation::West);
        // test valeur pars défaut
        assert_matches!(Orientation::from_str("Jsp"), Orientation::North);
    }
    #[test]
    fn test_deplacement() {
        let map = Grille {
            largeur: 5,
            longueur: 5,
        };
        let mut robs = Robots { liste: Vec::new() };
        let rob1 = Robot {
            orientation: Orientation::East,
            id: 1,
            pos_x: 0,
            pos_y: 0,
            instructions: vec![Action::F, Action::R, Action::R, Action::F],
        };
        let rob2 = Robot {
            orientation: Orientation::South,
            id: 2,
            pos_x: 3,
            pos_y: 3,
            instructions: vec![Action::F, Action::L, Action::L, Action::F],
        };
        robs.liste.push(rob2);
        robs.liste.push(rob1);
        deplacement(&map, &mut robs);
        assert_eq!(robs.liste[1].id, 1);
        assert_eq!(robs.liste[1].pos_x, 0);
        assert_eq!(robs.liste[1].pos_y, 0);
        assert_eq!(robs.liste[0].pos_x, 3);
        assert_eq!(robs.liste[0].pos_y, 3);
        assert_matches!(robs.liste[1].orientation, Orientation::West);
        assert_matches!(robs.liste[0].orientation, Orientation::North);
    }
    #[test]
    fn test_collision() {
        let map = Grille {
            largeur: 5,
            longueur: 5,
        };
        let mut robs = Robots { liste: Vec::new() };
        let rob1 = Robot {
            orientation: Orientation::North,
            id: 1,
            pos_x: 0,
            pos_y: 1,
            instructions: vec![Action::F],
        };
        let rob2 = Robot {
            orientation: Orientation::North,
            id: 2,
            pos_x: 0,
            pos_y: 0,
            instructions: vec![Action::R],
        };
        robs.liste.push(rob2);
        robs.liste.push(rob1);
        deplacement(&map, &mut robs);
        assert_eq!(robs.liste[1].id, 1);
        assert_eq!(robs.liste[1].pos_x, 0);
        // le Robot d'id 1 ne devrait pas bouger.
        assert_eq!(robs.liste[1].pos_y, 1);
        assert_matches!(robs.liste[1].orientation, Orientation::North);
    }
    #[test]
    fn test_file() {
        let mut robs = Robots { liste: Vec::new() };

        let mut file = File::open("param.txt").expect("Unable to open the file");
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .expect("Unable to read the file");

        let map: Grille = match setup(contents, &mut robs) {
            Ok(g) => g,
            Err(e) => panic!(e),
        };

        assert_matches!(
            map,
            Grille {
                longueur: 5,
                largeur: 5
            }
        );
        assert_eq!(robs.liste[0].pos_x, 1);
        assert_eq!(robs.liste[0].pos_y, 1);
        assert_matches!(robs.liste[0].orientation, Orientation::South);
    }
}
